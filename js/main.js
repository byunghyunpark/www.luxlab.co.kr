var mobileKeyWords = new Array('iPhone', 'iPod', 'BlackBerry', 'Android', 'Windows CE', 'LG', 'MOT', 'SAMSUNG', 'SonyEricsson');
for (var word in mobileKeyWords){
    if (navigator.userAgent.match(mobileKeyWords[word]) != null){
            location.href = "https://m.luxlab.co.kr/";   
    }else{
       
    }
}

$('.carousel').carousel({
  interval: 3000
});

function scrollMoving (seq) {

    var offset = $('.luxlab-section-' + seq).offset();
    $('html, body')
        .animate(
            { scrollTop : offset.top - 55 }, 500
        );

}

var carouselItem01 = document.getElementById('carousel-inner-01');
var carouselItem02 = document.getElementById('carousel-inner-02');

var carouselText01 = document.getElementById('luxlab-sub-01-carousel-text').children;
var carouselText02 = document.getElementById('luxlab-sub-02-carousel-text').children;
var carouselItemChildren, carouselItemActive, i, l;

$('#luxlab-sub-01-carousel').on('slide.bs.carousel', function () {
  window.setTimeout(function () {
    carouselItemChildren = carouselItem01.children;
    for ( i = 0, l = carouselItemChildren.length; i < l; i++ ) {
      if ( carouselItemChildren[i].classList.contains('next') ||
           carouselItemChildren[i].classList.contains('prev')) {
        if ( carouselText01[i].className !== 'active' ) {
          carouselText01[i].className = 'active';
        }
      } else {
        carouselText01[i].classList.remove('active');
      }
    }
  }, 100);
});

$('#luxlab-sub-02-carousel').on('slide.bs.carousel', function () {
  window.setTimeout(function () {
    carouselItemChildren = carouselItem02.children;
    for ( i = 0, l = carouselItemChildren.length; i < l; i++ ) {
      if ( carouselItemChildren[i].classList.contains('next') ||
           carouselItemChildren[i].classList.contains('prev')) {
        if ( carouselText02[i].className !== 'active' ) {
          carouselText02[i].className = 'active';
        }
      } else {
        carouselText02[i].classList.remove('active');
      }
    }
  }, 100);
});
